document.addEventListener('DOMContentLoaded', () => {
    const form = document.querySelector('#update-form');
    const currentPasswordInput = document.querySelector('#password');
    const newEmailInput = document.querySelector('#new-email');
    const newPasswordInput = document.querySelector('#new-password');
    const newPasswordConfirmInput = document.querySelector('#new-password-validate');
    const submitButton = document.querySelector('#update-button');
    // Get a reference to the error message container
    const errorMessageContainer = document.getElementById('error-message');

    // Function to show the error message
    function showError(message) {
        errorMessageContainer.textContent = message;
        errorMessageContainer.style.display = 'block'; // Set the display property to 'block'
    }

    // Function to hide the error message
    function hideError() {
        errorMessageContainer.textContent = '';
        errorMessageContainer.style.display = 'none'; // Set the display property to 'none'
    }
    hideError();

    const validateForm = () => {
        const newPasswordsMatch = newPasswordInput.value === newPasswordConfirmInput.value;
        const formValid = (newEmailInput.value || currentPasswordInput.value) && newPasswordsMatch;
        submitButton.disabled = !formValid;
        if (newPasswordsMatch) {
            hideError();
        } else {
            showError("New passwords do not match.");
        }
    };

    newEmailInput.addEventListener('input', validateForm);
    newPasswordInput.addEventListener('input', validateForm);
    newPasswordConfirmInput.addEventListener('input', validateForm);
    currentPasswordInput.addEventListener('input', validateForm);

    form.addEventListener('submit', async (event) => {
        event.preventDefault();

        const updateRequest = {
            password: currentPasswordInput.value,
            new_email: newEmailInput.value || null,
            new_password: newPasswordInput.value || null,
        };

        try {
            const response = await fetch('/ovarit_auth/update', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(updateRequest),
            });

            if (response.ok) {
                window.location.href = '/';
            } else {
                showError('Error updating user. Please try again.');
            }
        } catch (error) {
            showError('Error updating user. Please try again.');
        }
    });
});
