const registrationForm = document.getElementById("registration-form");
const passwordInput = document.getElementById("password");
const validatePasswordInput = document.getElementById("validate_password");
const submitButton = registrationForm.querySelector("button[type='submit']");

// Get a reference to the error message container
const errorMessageContainer = document.getElementById('error-message');

// Function to show the error message
function showError(message) {
    errorMessageContainer.textContent = message;
    errorMessageContainer.style.display = 'block'; // Set the display property to 'block'
}

// Function to hide the error message
function hideError() {
    errorMessageContainer.textContent = '';
    errorMessageContainer.style.display = 'none'; // Set the display property to 'none'
}
hideError();

function checkFormValidity() {
    const allFieldsFilled = Array.from(registrationForm.querySelectorAll("input")).every((input) => input.value.trim() !== "");
    const passwordsMatch = passwordInput.value === validatePasswordInput.value;
    const passwordComplexEnough = isPasswordComplexEnough(passwordInput.value);

    if (allFieldsFilled && passwordsMatch && passwordComplexEnough) {
        submitButton.disabled = false;
        hideError();
    } else {
        submitButton.disabled = true;

        if (!passwordsMatch && validatePasswordInput.value) {
            showError("Passwords do not match.");
        } else if (!passwordComplexEnough && passwordInput.value) {
            showError("Password is not complex enough. It must be at least 12 characters long and contain at least 3 of the following: lowercase letters, uppercase letters, digits, and special characters.");
        } else {
            hideError();
        }
    }
}

registrationForm.addEventListener("input", checkFormValidity);

registrationForm.addEventListener("submit", async (e) => {
    e.preventDefault();

    const newUser = {
        invite_code: document.getElementById("invite_code").value,
        username: document.getElementById("username").value,
        email: document.getElementById("email").value,
        password: passwordInput.value
    };

    const response = await fetch("/ovarit_auth/create", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(newUser)
    });

    if (response.ok) {
        const { code, uid } = await response.json();
        window.location.href = "/confirmation?delete_code=" + code + "&uid=" + uid + "&email=" + newUser.email + "&username=" + newUser.username;
    } else {
        showError("Error: " + response.statusText);
    }
});

function isPasswordComplexEnough(password) {
    // Check for password length
    if (password.length < 12) {
        return false;
    }

    // Check for lowercase letters
    const hasLowerCase = /[a-z]/.test(password);

    // Check for uppercase letters
    const hasUpperCase = /[A-Z]/.test(password);

    // Check for digits
    const hasDigit = /[0-9]/.test(password);

    // Check for special characters
    const hasSpecialChar = /[!@#$%^&*(),.?":{}|<>]/.test(password);

    // Count the number of character types present in the password
    const characterTypes = [hasLowerCase, hasUpperCase, hasDigit, hasSpecialChar];
    const characterTypesCount = characterTypes.filter(Boolean).length;

    // Ensure that the password contains at least 3 character types
    if (characterTypesCount >= 3) {
        return true;
    } else {
        return false;
    }
}
