const loginForm = document.getElementById("login-form");
// Get a reference to the error message container
const errorMessageContainer = document.getElementById('error-message');

// Function to show the error message
function showError(message) {
    errorMessageContainer.textContent = message;
    errorMessageContainer.style.display = 'block'; // Set the display property to 'block'
}

// Function to hide the error message
function hideError() {
    errorMessageContainer.textContent = '';
    errorMessageContainer.style.display = 'none'; // Set the display property to 'none'
}
hideError();

loginForm.addEventListener("submit", async (e) => {
    e.preventDefault();

    const authenticate = {
        username: document.getElementById("username").value,
        password: document.getElementById("password").value,
        remember_me: document.getElementById('remember_me').checked
    };

    const response = await fetch("/ovarit_auth/authenticate", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(authenticate)
    });

    if (response.ok) {
        window.location.href = "/";
    } else {
        showError("Error: " + response.statusText);
    }
});
