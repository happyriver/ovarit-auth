mod mixed_user_store;
mod pinpoint_email;

use axum::{
    body::Body,
    extract::{Path, State},
    http::{
        header::{self, HeaderName},
        HeaderMap, HeaderValue, StatusCode,
    },
    middleware::from_fn_with_state,
    response::{IntoResponse, Redirect, Response},
    routing::{get, post},
    Json, Router,
};
use eyre::{eyre, Result};
use mixed_user_store::MixedUserStore;
use ovarit_auth::{
    authenticate::{self, AuthenticationError},
    create::{self, CreateError},
    flask_session::{encode_session, remember_me, Session},
    update,
    users::{DataStore, UserContact, UserError, UserState},
};
use pinpoint_email::PinpointContact;
use serde::Deserialize;
use serde_json::json;
use sqlx::PgPool;
use std::net::SocketAddr;
use std::{any::Any, sync::Arc, time::Duration};
use tower_http::{
    catch_panic::CatchPanicLayer,
    compression::CompressionLayer,
    propagate_header::PropagateHeaderLayer,
    request_id::{MakeRequestUuid, SetRequestIdLayer},
    services::{ServeDir, ServeFile},
    set_header::SetResponseHeaderLayer,
    trace::{DefaultMakeSpan, DefaultOnFailure, DefaultOnRequest, DefaultOnResponse, TraceLayer},
    LatencyUnit,
};
use tracing::{error, Level};
use tracing_subscriber::EnvFilter;
use url::Url;

#[derive(Clone)]
struct AppState {
    user_store: Arc<dyn DataStore + Send + Sync>,
    user_contact: Arc<dyn UserContact + Send + Sync>,
}

fn create_pool() -> Result<PgPool> {
    let host = std::env::var("DATABASE_HOST")?;
    let port = std::env::var("DATABASE_PORT")?;
    let password = std::env::var("DATABASE_PASSWORD")?;
    let user = std::env::var("DATABASE_USER")?;
    let name = std::env::var("DATABASE_NAME")?;

    let mut database = Url::parse(&format!("postgresql://{}", host))?;
    database.set_path(&name);
    database
        .set_username(&user)
        .map_err(|_| eyre!("Username not allowed"))?;
    database
        .set_port(port.parse().ok())
        .map_err(|_| eyre!("Port not allowed"))?;
    database
        .set_password(Some(&password))
        .map_err(|_| eyre!("Password not allowed"))?;

    PgPool::connect_lazy(database.as_str())
        .map_err(|e| eyre!(e).wrap_err("Failed to connect to database"))
}

#[tokio::main]

async fn main() {
    let format = tracing_subscriber::fmt()
        .json()
        .with_env_filter(EnvFilter::from_default_env())
        .finish();

    tracing::subscriber::set_global_default(format).unwrap();
    let shared_config = aws_config::load_from_env().await;

    // If we are built in release mode, use the real database
    let state = if !cfg!(debug_assertions) {
        AppState {
            user_store: Arc::new(MixedUserStore {
                sql_pool: create_pool().unwrap(),
            }),
            user_contact: Arc::new(PinpointContact {
                client: aws_sdk_pinpoint::Client::new(&shared_config),
            }),
        }
    } else {
        AppState {
            user_store: Arc::new(in_memory::InMemoryDataStore::default()),
            user_contact: Arc::new(in_memory::StdOutUserContact),
        }
    };

    let x_request_id = HeaderName::from_static("x-request-id");
    let cache_control = HeaderName::from_static("cache-control");

    let app = Router::new()
        .route("/ovarit_auth/authenticate", post(login))
        .route("/ovarit_auth/create", post(create))
        .route("/ovarit_auth/create/validate/:uid/:code", get(validate))
        .route("/ovarit_auth/create/resend", post(resend))
        .route("/ovarit_auth/reset", post(reset))
        .route("/ovarit_auth/reset/complete", post(reset_complete))
        .route("/ovarit_auth/update", post(update))
        .route("/ovarit_auth/delete", post(delete))
        .route_layer(from_fn_with_state(
            rate_limiter::RateLimiter {
                max_requests: 10,
                duration: Duration::from_secs(60),
                requests: Default::default(),
            },
            rate_limiter::rate_limiter,
        ))
        .route_service("/", ServeFile::new("static/root.html"))
        .route_service("/login", ServeFile::new("static/login.html"))
        .route_service("/register", ServeFile::new("static/register.html"))
        .route_service("/confirmation", ServeFile::new("static/confirmation.html"))
        .route_service("/reset", ServeFile::new("static/reset.html"))
        .route_service(
            "/reset_complete",
            ServeFile::new("static/reset_complete.html"),
        )
        .route_service("/resend", ServeFile::new("static/resend.html"))
        .route_service("/update", ServeFile::new("static/update.html"))
        .nest_service("/ovarit_auth/static", ServeDir::new("static"))
        .route("/health", get(health))
        .layer(
            TraceLayer::new_for_http()
                .make_span_with(DefaultMakeSpan::new().include_headers(true))
                .on_request(DefaultOnRequest::new().level(Level::INFO))
                .on_response(
                    DefaultOnResponse::new()
                        .level(Level::INFO)
                        .include_headers(true)
                        .latency_unit(LatencyUnit::Millis),
                )
                .on_failure(DefaultOnFailure::new().level(Level::INFO)),
        )
        .layer(CompressionLayer::new())
        .layer(PropagateHeaderLayer::new(x_request_id.clone()))
        .layer(SetRequestIdLayer::new(
            x_request_id.clone(),
            MakeRequestUuid,
        ))
        .layer(SetResponseHeaderLayer::if_not_present(
            cache_control,
            Some(HeaderValue::from_static("no-store")),
        ))
        .layer(CatchPanicLayer::custom(|err: Box<dyn Any + Send>| {
            let details = if let Some(s) = err.downcast_ref::<String>() {
                s.clone()
            } else if let Some(s) = err.downcast_ref::<&str>() {
                s.to_string()
            } else {
                "Unknown panic message".to_string()
            };

            error!("Panic: {details}");

            let body = serde_json::json!({
                "error": {
                    "kind": "panic",
                    "details": details,
                }
            });
            let body = serde_json::to_string(&body).unwrap();

            Response::builder()
                .status(StatusCode::INTERNAL_SERVER_ERROR)
                .header(header::CONTENT_TYPE, "application/json")
                .body(Body::from(body))
                .unwrap()
        }))
        .with_state(state);

    let addr = SocketAddr::from(([0, 0, 0, 0], 3000));

    tracing::info!("Listening on {addr}");

    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

async fn health() -> &'static str {
    "OK"
}

#[axum_macros::debug_handler]
#[tracing::instrument(skip(state))]
async fn login(
    State(state): State<AppState>,
    Json(login): Json<authenticate::Authenticate>,
) -> Response {
    let rm = login.remember_me;
    let res = authenticate::authenticate(login, &state.user_store).await;

    if let Err(ref e) = res {
        error!("Failed to authenticate: {e}");
    }

    match res {
        Ok(user) => {
            match user.state {
                UserState::Unverified { .. } => {
                    return (
                        StatusCode::FORBIDDEN,
                        Json(json! ({"reason": "Please finish email verification"})),
                    )
                        .into_response();
                }
                UserState::Verified | UserState::Reset { .. } | UserState::NewEmail { .. } => (),
                UserState::Banned => {
                    return (
                        StatusCode::FORBIDDEN,
                        Json(json! ({ "reason" : "You have been banned from the site."})),
                    )
                        .into_response();
                }
                UserState::Deleted => {
                    return (
                        StatusCode::NOT_FOUND,
                        Json(json!({"reason": "Your account has been deleted."})),
                    )
                        .into_response()
                }
            };

            let session_result = encode_session(&Session {
                _fresh: true,
                remember_me: false,
                _id: None,
                language: None,
                _user_id: user.uid.clone(),
                csrf_token: uuid::Uuid::new_v4().to_string(),
            });

            let session = match session_result {
                Ok(s) => s,
                Err(e) => {
                    return (e.0, Json(json! ({"reason": "Session un-encoded"}))).into_response()
                }
            };

            let mut headers = HeaderMap::new();
            headers.insert(
                header::SET_COOKIE,
                HeaderValue::from_str(&format!("session={session}; Path=/; HttpOnly")).unwrap(),
            );

            if rm {
                let rm_cookie = remember_me(user.uid);
                headers.insert(
                    header::SET_COOKIE,
                    HeaderValue::from_str(&format!(
                        "remember_token={rm_cookie}; Path=/; HttpOnly; Max-Age=31536000"
                    ))
                    .unwrap(),
                );
            };

            (StatusCode::ACCEPTED, headers, ()).into_response()
        }
        Err(AuthenticationError::BadPassword) => (
            StatusCode::UNAUTHORIZED,
            Json(json! ({ "reason" : "Invalid password"})),
        )
            .into_response(),
        Err(
            AuthenticationError::Unverified | AuthenticationError::User(UserError::InvalidState),
        ) => (
            StatusCode::FORBIDDEN,
            Json(json! ({ "reason" : "User state does not permit login"})),
        )
            .into_response(),
        Err(AuthenticationError::User(UserError::NotFound)) => (
            StatusCode::NOT_FOUND,
            Json(json! ({ "reason" : "User not found"})),
        )
            .into_response(),
        Err(AuthenticationError::User(UserError::PasswordHashingError(e))) => {
            error!("Failed to authenticate due to password hashing: {e}");
            (
                StatusCode::SERVICE_UNAVAILABLE,
                Json(json!({"reason": "An internal error occurred"})),
            )
                .into_response()
        }
        Err(AuthenticationError::User(
            UserError::Exists
            | UserError::BadCode
            | UserError::BannedEmailDomain
            | UserError::InvalidEmail
            | UserError::InvalidUsername
            | UserError::PasswordComplexity
            | UserError::RegistrationDisabled,
        )) => {
            panic!("Only in write path")
        }
        Err(AuthenticationError::User(UserError::DBError)) => (
            StatusCode::SERVICE_UNAVAILABLE,
            Json(json!({"reason": "An internal error occurred"})),
        )
            .into_response(),
    }
}

#[tracing::instrument(skip(state))]
async fn create(
    State(state): State<AppState>,
    Json(create): Json<create::NewUser>,
) -> impl IntoResponse {
    let res = create::create(create, &state.user_store, &state.user_contact).await;

    if let Err(ref e) = res {
        error!("Failed to create user: {e}");
    }

    match res {
        Ok((uid, code)) => (
            StatusCode::CREATED,
            Json(json!({ "uid": uid, "code": code })),
        ),
        Err(CreateError::EmailTaken | CreateError::User(UserError::Exists)) => (
            StatusCode::CONFLICT,
            Json(json!({ "reason": "Email or username already in use" })),
        ),
        Err(CreateError::User(UserError::BadCode)) => (
            StatusCode::BAD_REQUEST,
            Json(json!({ "reason": "Invalid invitation code" })),
        ),
        Err(CreateError::User(UserError::BannedEmailDomain)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "Email domain not allowed" })),
        ),
        Err(CreateError::User(UserError::InvalidEmail)) => (
            StatusCode::BAD_REQUEST,
            Json(json!({ "reason": "Invalid email address" })),
        ),
        Err(CreateError::User(UserError::InvalidUsername)) => (
            StatusCode::BAD_REQUEST,
            Json(json!({ "reason": "Invalid username" })),
        ),
        Err(CreateError::User(UserError::PasswordComplexity)) => (
            StatusCode::BAD_REQUEST,
            Json(json!({ "reason": "Password does not meet complexity requirements" })),
        ),
        Err(CreateError::User(UserError::RegistrationDisabled)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "Registration is currently disabled" })),
        ),
        Err(CreateError::User(UserError::NotFound)) => {
            panic!("Of course it's not found")
        }
        Err(CreateError::Email(_)) => (StatusCode::BAD_GATEWAY, {
            Json(json!({ "reason": "Email service error" }))
        }),
        Err(CreateError::User(UserError::PasswordHashingError(_))) => (
            StatusCode::SERVICE_UNAVAILABLE,
            Json(json!({ "reason": "An internal error occurred" })),
        ),
        Err(CreateError::User(UserError::InvalidState)) => {
            panic!("Only in update path")
        }
        Err(CreateError::User(UserError::DBError)) => (
            StatusCode::SERVICE_UNAVAILABLE,
            Json(json!({ "reason": "An internal error occurred" })),
        ),
    }
}

#[tracing::instrument(skip(state))]
async fn validate(
    Path((uid, code)): Path<(String, String)>,
    State(state): State<AppState>,
) -> Response {
    let res = create::validate(uid, code, &state.user_store).await;

    if let Err(ref e) = res {
        error!("Failed to validate user: {e}");
    }

    match res {
        Ok(_) => Redirect::to("/login").into_response(),
        Err(create::ValidateError::NoKey)
        | Err(create::ValidateError::User(UserError::NotFound | UserError::Exists)) => (
            StatusCode::NOT_FOUND,
            Json(json!({ "reason": "User not found or already validated" })),
        )
            .into_response(),
        Err(create::ValidateError::User(UserError::InvalidState)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "User state does not permit validation" })),
        )
            .into_response(),
        Err(create::ValidateError::User(UserError::PasswordHashingError(_))) => (
            StatusCode::SERVICE_UNAVAILABLE,
            Json(json!({ "reason": "An internal error occurred" })),
        )
            .into_response(),
        Err(create::ValidateError::User(
            UserError::DBError
            | UserError::BadCode
            | UserError::BannedEmailDomain
            | UserError::InvalidEmail
            | UserError::InvalidUsername
            | UserError::PasswordComplexity
            | UserError::RegistrationDisabled,
        )) => (
            StatusCode::SERVICE_UNAVAILABLE,
            Json(json!({ "reason": "An internal error occurred" })),
        )
            .into_response(),
    }
}

#[tracing::instrument(skip(state))]
async fn resend(
    State(state): State<AppState>,
    Json(req): Json<create::ResendRequest>,
) -> impl IntoResponse {
    let res = create::resend(req, &state.user_store, &state.user_contact).await;

    if let Err(ref e) = res {
        error!("Failed to resend validation email: {e}");
    }

    match res {
        Ok(_uid) => (
            StatusCode::OK,
            Json(json!({ "reason": "Email sent successfully" })),
        ),
        Err(create::ResendError::EmailError(_)) => (
            StatusCode::GATEWAY_TIMEOUT,
            Json(json!({ "reason": "Failed to send email, please try again later" })),
        ),
        Err(create::ResendError::User(_) | create::ResendError::UserVerified) => (
            StatusCode::OK,
            Json(json!({ "reason": "Email sent successfully or user already verified" })),
        ),
    }
}

#[tracing::instrument(skip(state))]
async fn reset(
    State(state): State<AppState>,
    Json(req): Json<update::ResetRequest>,
) -> impl IntoResponse {
    let res = update::start_reset(req, &state.user_store, &state.user_contact).await;
    if let Err(ref e) = res {
        error!("Failed to start reset: {e}");
    }
    match res {
        Ok(_) => (StatusCode::OK, ()),
        Err(update::ResetError::User(_)) => (StatusCode::OK, ()),
        Err(update::ResetError::Contact(_)) => (StatusCode::OK, ()),
        Err(update::ResetError::Authenticate(_)) => (StatusCode::OK, ()),
    }
}

#[tracing::instrument(skip(state))]
async fn reset_complete(
    State(state): State<AppState>,
    Json(req): Json<update::CompleteReset>,
) -> impl IntoResponse {
    let res = update::complete_reset(req, &state.user_store).await;

    if let Err(ref e) = res {
        error!("Failed to complete reset: {e}");
    }

    match res {
        Ok(_) => (
            StatusCode::OK,
            Json(json!({ "reason": "Password reset successful" })),
        ),
        Err(update::ResetError::User(
            UserError::DBError | UserError::BadCode | UserError::PasswordHashingError(_),
        )) => (
            StatusCode::INTERNAL_SERVER_ERROR,
            Json(json!({ "reason": "An internal error occurred" })),
        ),
        Err(update::ResetError::User(UserError::InvalidState)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "Invalid user state for password reset" })),
        ),
        Err(update::ResetError::User(UserError::NotFound)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "User not found" })),
        ),
        Err(update::ResetError::User(UserError::Exists)) => (
            StatusCode::CONFLICT,
            Json(json!({ "reason": "User already exists" })),
        ),
        Err(update::ResetError::Authenticate(_)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "Authentication failed" })),
        ),
        Err(
            update::ResetError::Contact(_)
            | update::ResetError::User(
                UserError::BannedEmailDomain
                | UserError::InvalidEmail
                | UserError::InvalidUsername
                | UserError::PasswordComplexity
                | UserError::RegistrationDisabled,
            ),
        ) => {
            unimplemented!("This should only happen on the start, not the complete")
        }
    }
}

#[derive(Deserialize, Debug)]
pub struct UpdateRequest {
    password: String,
    new_email: Option<String>,
    new_password: Option<String>,
}

#[tracing::instrument(skip(state))]
#[axum_macros::debug_handler]
async fn update(
    session: Session,
    State(state): State<AppState>,
    Json(req): Json<UpdateRequest>,
) -> impl IntoResponse {
    println!("Updating user: {:#?}", session);
    let res = update::update(
        update::UpdateRequest {
            uid: session._user_id,
            password: req.password,
            new_email: req.new_email,
            new_password: req.new_password,
        },
        &state.user_store,
        &state.user_contact,
    )
    .await;

    if let Err(ref e) = res {
        error!("Failed to update user: {e}");
    }

    match res {
        Ok(_) | Err(update::ResetError::User(UserError::RegistrationDisabled)) => (
            StatusCode::OK,
            Json(json!({ "reason": "User update successful" })),
        ),
        Err(update::ResetError::Authenticate(AuthenticationError::BadPassword)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "Incorrect password" })),
        ),
        Err(update::ResetError::User(UserError::NotFound)) => (
            StatusCode::NOT_FOUND,
            Json(json!({ "reason": "User not found" })),
        ),
        Err(update::ResetError::User(UserError::InvalidState)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "Invalid user state for update" })),
        ),
        Err(update::ResetError::User(UserError::PasswordHashingError(_)))
        | Err(update::ResetError::User(UserError::DBError)) => (
            StatusCode::SERVICE_UNAVAILABLE,
            Json(json!({ "reason": "An internal error occurred" })),
        ),
        Err(update::ResetError::Contact(_)) => (
            StatusCode::GATEWAY_TIMEOUT,
            Json(json!({ "reason": "Contact service not available" })),
        ),
        Err(update::ResetError::User(UserError::InvalidEmail)) => (
            StatusCode::BAD_REQUEST,
            Json(json!({ "reason": "Invalid email" })),
        ),
        Err(update::ResetError::User(UserError::InvalidUsername)) => (
            StatusCode::BAD_REQUEST,
            Json(json!({ "reason": "Invalid username" })),
        ),
        Err(update::ResetError::User(UserError::PasswordComplexity)) => (
            StatusCode::BAD_REQUEST,
            Json(json!({ "reason": "Password complexity requirements not met" })),
        ),
        Err(update::ResetError::User(UserError::Exists))
        | Err(update::ResetError::User(UserError::BadCode))
        | Err(update::ResetError::Authenticate(AuthenticationError::User(_))) => {
            panic!("This is only in the write path")
        }
        Err(update::ResetError::Authenticate(AuthenticationError::Unverified))
        | Err(update::ResetError::User(UserError::BannedEmailDomain)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "Forbidden operation" })),
        ),
    }
}

#[tracing::instrument(skip(state))]
async fn delete(
    State(state): State<AppState>,
    Json(req): Json<update::DeleteRequest>,
) -> impl IntoResponse {
    let res = update::delete(req, &state.user_store).await;
    if let Err(ref e) = res {
        error!("Failed to delete user: {e}");
    }
    match res {
        Ok(_) => (
            StatusCode::OK,
            Json(json!({ "reason": "User deleted successfully" })),
        ),
        Err(update::ResetError::User(UserError::NotFound)) => (
            StatusCode::NOT_FOUND,
            Json(json!({ "reason": "User not found" })),
        ),
        Err(update::ResetError::User(UserError::InvalidState)) => (
            StatusCode::FORBIDDEN,
            Json(json!({ "reason": "Invalid user state for deletion" })),
        ),
        _ => (
            StatusCode::INTERNAL_SERVER_ERROR,
            Json(json!({ "reason": "An internal error occurred" })),
        ),
    }
}

mod in_memory {
    use async_trait::async_trait;
    use ovarit_auth::users::{ContactType, DataStore, EmailError, User, UserContact, UserError};
    use std::collections::HashMap;
    use std::sync::Arc;
    use tokio::sync::RwLock;

    #[derive(Default)]
    pub struct InMemoryDataStore {
        users: Arc<RwLock<HashMap<String, User>>>,
    }

    #[async_trait]
    impl DataStore for InMemoryDataStore {
        async fn get_user_by_uid(&self, uid: &str) -> Result<User, UserError> {
            let users = self.users.read().await;
            users.get(uid).cloned().ok_or(UserError::NotFound)
        }

        async fn get_user_by_email(&self, email: &str) -> Result<Vec<User>, UserError> {
            let users = self.users.read().await;
            let result: Vec<User> = users
                .values()
                .filter(|user| user.email == email)
                .cloned()
                .collect();
            Ok(result)
        }

        async fn get_user_by_username(&self, username: &str) -> Result<User, UserError> {
            let users = self.users.read().await;
            users
                .values()
                .find(|user| user.username == username)
                .cloned()
                .ok_or(UserError::NotFound)
        }

        async fn write_user(&self, _code: &str, user: &User) -> Result<(), UserError> {
            let mut users = self.users.write().await;
            if users.contains_key(&user.uid)
                || users.iter().any(|(_, u)| u.username == user.username)
            {
                return Err(UserError::Exists);
            }
            users.insert(user.uid.clone(), user.clone());
            Ok(())
        }

        async fn update_user(&self, user: &User) -> Result<(), UserError> {
            let mut users = self.users.write().await;
            if let Some(existing_user) = users.get_mut(&user.uid) {
                *existing_user = user.clone();
                Ok(())
            } else {
                Err(UserError::NotFound)
            }
        }

        async fn delete_user(&self, uid: &str) -> Result<(), UserError> {
            let mut users = self.users.write().await;
            if users.remove(uid).is_some() {
                Ok(())
            } else {
                Err(UserError::NotFound)
            }
        }
    }

    // Your existing ContactType and UserContact trait definitions...

    pub struct StdOutUserContact;

    #[async_trait]
    impl UserContact for StdOutUserContact {
        async fn send_email(
            &self,
            ty: ContactType,
            email: &str,
            uid: &str,
            code: &str,
        ) -> Result<(), EmailError> {
            let message = match ty {
                ContactType::Verify => "Please verify your email address",
                ContactType::Reset => "Please reset your password",
                ContactType::Update => "Please update your email address",
            };

            println!("Sending email to: {}", email);
            println!("UID: {}", uid);
            println!("Message: {}", message);
            println!("Code: {}", code);
            match ty {
                ContactType::Verify | ContactType::Update => {
                    println!("Url: http://localhost:3000/ovarit_auth/create/validate/{uid}/{code}")
                }
                ContactType::Reset => {
                    println!("Url: http://localhost:3000/reset_complete?username={uid}&code={code}")
                }
            }
            println!("-------------------------");

            Ok(())
        }
    }
}

mod rate_limiter {
    use axum::{
        body::HttpBody,
        extract::State,
        http::{Request, StatusCode},
        middleware::Next,
        response::{IntoResponse, Response},
    };
    use std::{
        collections::HashMap,
        sync::Arc,
        time::{Duration, Instant},
    };
    use tokio::sync::RwLock;
    use tracing::error;

    #[derive(Debug, Clone)]
    pub struct RateLimiter {
        pub max_requests: usize,
        pub duration: Duration,
        pub requests: Arc<RwLock<HashMap<String, Vec<Instant>>>>,
    }

    pub async fn rate_limiter<B: HttpBody + Send + 'static>(
        State(state): State<RateLimiter>,
        request: Request<B>,
        next: Next<B>,
    ) -> Response {
        let ip = request
            .headers()
            .get("X-Forwarded-For")
            .and_then(|x| x.to_str().ok());

        if let Some(ip) = ip {
            let mut requests = state.requests.write().await;
            let times = requests.entry(ip.to_string()).or_insert(Vec::new());

            times.push(Instant::now());
            times.retain(|t| t.elapsed() < state.duration);

            if times.len() > state.max_requests {
                error!("Rate limit exceeded for ip: {}", ip);
                return (StatusCode::TOO_MANY_REQUESTS, ()).into_response();
            };
        };

        next.run(request).await
    }
}
