use std::{fmt::Debug, sync::Arc};

use serde::Deserialize;
use thiserror::Error;
use uuid::Uuid;

use crate::users::{self, User, UserState};

#[derive(Error, Debug)]
pub enum CreateError {
    #[error("Error in the users subsystem: {0}")]
    User(#[from] users::UserError),
    #[error("Error in the email subsystem: {0}")]
    Email(#[from] users::EmailError),

    #[error("email already taken")]
    EmailTaken,
}

#[derive(Deserialize)]
pub struct NewUser {
    invite_code: String,
    username: String,
    email: String,
    password: String,
}

impl Debug for NewUser {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("NewUser")
            .field("invite_code", &self.invite_code)
            .field("username", &self.username)
            .field("email", &self.email)
            .field("password", &"***")
            .finish()
    }
}

/// Returns the UID of the user in the system
#[tracing::instrument(skip(user_datastore, user_contact))]
pub async fn create(
    new_user: NewUser,
    user_datastore: &Arc<dyn users::DataStore + Send + Sync>,
    user_contact: &Arc<dyn users::UserContact + Send + Sync>,
) -> Result<(String, String), CreateError> {
    // Ensure password complexity by verifying it meets 3 of the following conditions:
    // 1. Contains an uppercase letter
    // 2. Contains a lowercase letter
    // 3. Contains a number
    // 4. Contains a special character
    // Additionally, the password must be at least 12 characters long.
    let mut complexity = 0;

    if new_user.password.chars().any(char::is_uppercase) {
        complexity += 1;
    }
    if new_user.password.chars().any(char::is_lowercase) {
        complexity += 1;
    }
    if new_user.password.chars().any(char::is_numeric) {
        complexity += 1;
    }
    if new_user
        .password
        .chars()
        .any(|c| "!@#$%^&*()_+-=[]{}|;':\",./<>?`~".contains(c))
    {
        complexity += 1;
    }

    if complexity < 3 || new_user.password.len() < 12 {
        return Err(CreateError::User(users::UserError::PasswordComplexity));
    }

    // Check if username is valid, it should be 3-64 characters long and only contain
    // alphanumeric characters, underscores and hyphens.
    if new_user.username.len() < 3 || new_user.username.len() > 64 {
        return Err(CreateError::User(users::UserError::InvalidUsername));
    }
    if !new_user
        .username
        .chars()
        .all(|c| c.is_ascii_alphanumeric() || c == '_' || c == '-')
    {
        return Err(CreateError::User(users::UserError::InvalidUsername));
    }

    // Check if email is valid
    if !new_user.email.contains('@') {
        return Err(CreateError::User(users::UserError::InvalidEmail));
    }
    // normalize the email address
    let new_user = NewUser {
        email: new_user.email.trim().to_lowercase(),
        ..new_user
    };

    let delete_code = Uuid::new_v4().as_hyphenated().to_string();
    let code = Uuid::new_v4().as_hyphenated().to_string();
    let user = users::User {
        uid: Uuid::new_v4().as_hyphenated().to_string(),
        username: new_user.username,
        email: new_user.email,
        parsed_hash: users::hash_password(&new_user.password)?,
        state: users::UserState::Unverified {
            delete: delete_code.clone(),
            code: code.clone(),
        },
    };
    user_datastore
        .write_user(new_user.invite_code.as_str(), &user)
        .await?;
    user_contact
        .send_email(
            users::ContactType::Verify,
            &user.email,
            &user.username,
            &code,
        )
        .await?;

    Ok((user.uid, delete_code))
}

#[derive(Error, Debug)]
pub enum ValidateError {
    #[error("Error in the users subsystem: {0}")]
    User(#[from] users::UserError),
    #[error("No Key")]
    NoKey,
}

#[tracing::instrument(skip(user_datastore))]
pub async fn validate(
    uid: String,
    key: String,
    user_datastore: &Arc<dyn users::DataStore + Send + Sync>,
) -> Result<(), ValidateError> {
    let user = user_datastore.get_user_by_username(&uid).await?;
    let updated_user = match user.state {
        UserState::Unverified { code, .. } if code == key => User {
            state: UserState::Verified,
            ..user
        },
        UserState::NewEmail { email, code } if code == key => User {
            email,
            state: UserState::Verified,
            ..user
        },
        _ => return Err(ValidateError::NoKey),
    };

    user_datastore.update_user(&updated_user).await?;

    Ok(())
}

#[derive(Error, Debug)]
pub enum ResendError {
    #[error("Error in the users subsystem: {0}")]
    User(#[from] users::UserError),
    #[error("Email Error: {0}")]
    EmailError(#[from] users::EmailError),
    #[error("User is already verified, nothing to resend")]
    UserVerified,
}

#[derive(Deserialize, Debug)]
pub struct ResendRequest {
    uid: String,
}

#[tracing::instrument(skip(user_datastore, user_contact))]
pub async fn resend(
    req: ResendRequest,
    user_datastore: &Arc<dyn users::DataStore + Send + Sync>,
    user_contact: &Arc<dyn users::UserContact + Send + Sync>,
) -> Result<(), ResendError> {
    let user = user_datastore.get_user_by_username(&req.uid).await?;
    let code = match user.state {
        UserState::Unverified { code, .. } => code,
        UserState::NewEmail { code, .. } => code,
        UserState::Reset { code } => code,
        UserState::Verified | UserState::Banned | UserState::Deleted => {
            return Err(ResendError::UserVerified)
        }
    };
    user_contact
        .send_email(
            users::ContactType::Verify,
            &user.email,
            &user.username,
            &code,
        )
        .await?;

    Ok(())
}

#[cfg(test)]
mod test {
    use std::{collections::HashMap, sync::Arc};

    use tokio::sync::Mutex;

    use crate::{
        authenticate::{Authenticate, AuthenticationError},
        users::{DataStore, User, UserContact, UserError},
    };

    use super::*;

    struct HashMapStore {
        map: Mutex<HashMap<String, User>>,
    }
    #[async_trait::async_trait]
    impl DataStore for HashMapStore {
        async fn get_user_by_uid(&self, uid: &str) -> Result<User, UserError> {
            self.map
                .lock()
                .await
                .get(uid)
                .cloned()
                .ok_or(UserError::NotFound)
        }
        async fn get_user_by_email(
            &self,
            _email: &str,
        ) -> Result<Vec<users::User>, users::UserError> {
            unimplemented!()
        }

        async fn get_user_by_username(&self, uid: &str) -> Result<users::User, UserError> {
            self.map
                .lock()
                .await
                .iter()
                .find(|(_, user)| user.username == uid)
                .map(|(_, user)| user.clone())
                .ok_or(UserError::NotFound)
        }

        async fn write_user(&self, _code: &str, user: &users::User) -> Result<(), UserError> {
            let mut map = self.map.lock().await;
            if map
                .values()
                .any(|stored_user| user.email == stored_user.email)
            {
                return Err(UserError::Exists);
            }
            map.insert(user.uid.clone(), user.clone());

            Ok(())
        }

        async fn update_user(&self, user: &users::User) -> Result<(), UserError> {
            let mut map = self.map.lock().await;

            let old_user = map.get_mut(&user.uid).ok_or(UserError::NotFound)?;

            *old_user = user.clone();

            Ok(())
        }

        async fn delete_user(&self, _uid: &str) -> Result<(), UserError> {
            self.map.lock().await.remove(_uid);
            Ok(())
        }
    }

    struct HashMapContact {
        map: Mutex<HashMap<String, String>>,
    }
    #[async_trait::async_trait]
    impl UserContact for HashMapContact {
        async fn send_email(
            &self,
            _ty: users::ContactType,
            _email: &str,
            uid: &str,
            code: &str,
        ) -> Result<(), users::EmailError> {
            self.map
                .lock()
                .await
                .insert(uid.to_string(), code.to_string());
            Ok(())
        }
    }

    #[tokio::test]
    async fn create_no_validate() {
        let hms = HashMapStore {
            map: Default::default(),
        };
        let store = Arc::new(hms);
        let contact = Arc::new(HashMapContact {
            map: Default::default(),
        });
        let password = "dumb_password".to_string();
        let email = "nobody@example.com".to_string();

        let (uid, _) = create(
            NewUser {
                invite_code: "1234".to_string(),
                username: "nobody".to_string(),
                email: email.clone(),
                password: password.clone(),
            },
            &(store.clone() as _),
            &(contact.clone() as _),
        )
        .await
        .unwrap();

        let result = crate::authenticate::authenticate(
            Authenticate {
                password,
                username: uid.clone(),
                remember_me: false,
            },
            &(store.clone() as _),
        )
        .await;

        assert_eq!(result, Err(AuthenticationError::Unverified));

        assert!(contact.map.lock().await.contains_key(&uid))
    }

    #[tokio::test]
    async fn create_resend() {
        let store = Arc::new(HashMapStore {
            map: Default::default(),
        });
        let contact = Arc::new(HashMapContact {
            map: Default::default(),
        });

        let contact2 = Arc::new(HashMapContact {
            map: Default::default(),
        });
        let password = "dumb_password".to_string();
        let email = "nobody@example.com".to_string();

        let (uid, _) = create(
            NewUser {
                invite_code: "1234".to_string(),
                username: "nobody".to_string(),
                email: email.clone(),
                password: password.clone(),
            },
            &(store.clone() as _),
            &(contact.clone() as _),
        )
        .await
        .unwrap();

        resend(
            ResendRequest { uid: uid.clone() },
            &(store.clone() as _),
            &(contact2.clone() as _),
        )
        .await
        .unwrap();

        assert_eq!(
            contact.map.lock().await.get(&uid).cloned(),
            contact2.map.lock().await.get(&uid).cloned()
        );
    }

    #[tokio::test]
    async fn create_validate() {
        let store = Arc::new(HashMapStore {
            map: Default::default(),
        });
        let contact = Arc::new(HashMapContact {
            map: Default::default(),
        });
        let password = "dumb_password".to_string();
        let email = "nobody@example.com".to_string();

        let (uid, _) = create(
            NewUser {
                invite_code: "1234".to_string(),
                username: "nobody".to_string(),
                email: email.clone(),
                password: password.clone(),
            },
            &(store.clone() as _),
            &(contact.clone() as _),
        )
        .await
        .unwrap();

        validate(
            uid.clone(),
            contact.map.lock().await.get(&uid).unwrap().to_owned(),
            &(store.clone() as _),
        )
        .await
        .unwrap();

        crate::authenticate::authenticate(
            Authenticate {
                password,
                username: uid,
                remember_me: false,
            },
            &(store as _),
        )
        .await
        .expect("Login should be successful");
    }
}
