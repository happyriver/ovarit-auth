use super::*;

use async_trait::async_trait;
use ovarit_auth::users::{self, User, UserState};
use sqlx::{types::Json, PgPool, Row};

#[derive(Clone)]
pub(crate) struct MixedUserStore {
    pub sql_pool: PgPool,
}

impl MixedUserStore {
    /// Helper function to fetch all banned email domains from the database
    /// and return them as a vector of strings.
    async fn get_banned_email_domains(&self) -> Result<Vec<String>, sqlx::Error> {
        Ok(
            sqlx::query("SELECT value FROM site_metadata where key = 'banned_email_domains'")
                .fetch_all(&self.sql_pool)
                .await?
                .into_iter()
                .map(|row| row.get::<String, _>(0))
                .collect::<Vec<_>>(),
        )
    }

    /// Helper function to check that site registration is enabled.
    async fn is_registration_enabled(&self) -> Result<bool, sqlx::Error> {
        Ok(
            sqlx::query("SELECT value FROM site_metadata where key = 'site.enable_registration'")
                .fetch_one(&self.sql_pool)
                .await?
                .get::<i8, _>(0)
                == 1,
        )
    }
}

// We have two database tables, one for user information for throat
// and one for user login information for ovarit-auth. We need to
// combine the two to get a full user object.
//
// Table structure is as follows:
//
// throat.user:
// uid: String
// name: String
// state: i8
//
// ovarit_auth.user:
// uid: String
// email: String
// parsed_hash: String
// state: String
//

#[async_trait]
impl users::DataStore for MixedUserStore {
    async fn get_user_by_uid(&self, uid: &str) -> Result<users::User, users::UserError> {
        // get uid, name, email, parsed_hash, state, and the state from throat.user
        let (uid, name, email, parsed_hash, state, tstate) =
            sqlx::query_as::<_, (String, String, String, String, Json<UserState>, i8)>(
                "SELECT u.uid, u.name, a.email, a.parsed_hash, a.state, u.state 
                    FROM public.user u 
                    INNER JOIN user_login a 
                        ON u.uid = a.uid 
                WHERE u.uid = $1",
            )
            .bind(uid)
            .fetch_optional(&self.sql_pool)
            .await
            .map_err(|_| users::UserError::DBError)?
            .ok_or(users::UserError::NotFound)?;

        Ok(User {
            uid,
            username: name,
            email,
            parsed_hash,
            state: if tstate == 0 {
                state.0
            } else if tstate == 10 {
                UserState::Deleted
            } else {
                UserState::Banned
            },
        })
    }

    #[tracing::instrument(skip(self))]
    async fn get_user_by_email(&self, email: &str) -> Result<Vec<users::User>, users::UserError> {
        // let (uid, name, email, parsed_hash, state, tstate) =
        let users = sqlx::query_as::<_, (String, String, String, String, Json<UserState>, i8)>(
            "SELECT u.uid, u.name, a.email, a.parsed_hash, a.state, u.state 
                FROM public.user u 
                INNER JOIN user_login a 
                    ON u.uid = a.uid 
            WHERE a.email = $1",
        )
        .bind(email.trim().to_lowercase())
        .fetch_all(&self.sql_pool)
        .await
        .map_err(|_| users::UserError::DBError)?;

        let users = users
            .into_iter()
            .map(|(uid, name, email, parsed_hash, state, tstate)| User {
                uid,
                username: name,
                email,
                parsed_hash,
                state: if tstate == 0 {
                    state.0
                } else if tstate == 10 {
                    UserState::Deleted
                } else {
                    UserState::Banned
                },
            })
            .collect::<Vec<_>>();
        Ok(users)
    }

    #[tracing::instrument(skip(self))]
    async fn get_user_by_username(&self, username: &str) -> Result<users::User, UserError> {
        // get uid, name, email, parsed_hash, state, and the state from throat.user
        let (uid, name, email, parsed_hash, state, tstate) =
            sqlx::query_as::<_, (String, String, String, String, Json<UserState>, i8)>(
                "SELECT u.uid, u.name, a.email, a.parsed_hash, a.state, u.state 
                    FROM public.user u 
                    INNER JOIN user_login a 
                        ON u.uid = a.uid 
                WHERE u.name = $1",
            )
            .bind(username)
            .fetch_optional(&self.sql_pool)
            .await
            .map_err(|_| users::UserError::DBError)?
            .ok_or(users::UserError::NotFound)?;

        Ok(User {
            uid,
            username: name,
            email,
            parsed_hash,
            state: if tstate == 0 {
                state.0
            } else if tstate == 10 {
                UserState::Deleted
            } else {
                UserState::Banned
            },
        })
    }

    #[tracing::instrument(skip(self))]
    async fn write_user(
        &self,
        code: &str,
        users::User {
            email,
            uid,
            username,
            parsed_hash,
            state,
        }: &users::User,
    ) -> Result<(), UserError> {
        if !self.is_registration_enabled().await.unwrap_or(false) {
            return Err(UserError::RegistrationDisabled);
        };

        self.get_banned_email_domains()
            .await
            .map_err(|_| UserError::DBError)?
            .iter()
            .find(|domain| email.ends_with(*domain))
            .map(|_| Err(UserError::BannedEmailDomain))
            .unwrap_or(Ok(()))?;

        let mut tx = self
            .sql_pool
            .begin()
            .await
            .map_err(|_| UserError::DBError)?;

        let state_code = match state {
            UserState::Banned => 10,
            UserState::Deleted => 5,
            _ => 0,
        };

        // Execute the merged SQL statement using CTEs
        let affected = sqlx::query(
            r#"
            WITH update_invite_code AS (
                -- Update the invite_code table
                UPDATE invite_code
                SET used = used + 1
                WHERE code = $1
                AND used < max_uses
                AND expires > NOW()
                RETURNING code
            ),
            insert_user AS (
                -- Insert a new row into the public.user table if the user does not already exist
                INSERT INTO public.user (uid, name, state)
                SELECT $2, $3, $4
                WHERE NOT EXISTS (SELECT 1 FROM public.user WHERE name = $3)
            ),
            insert_user_login AS (
                -- Insert a new row into the user_login table
                INSERT INTO user_login (uid, email, parsed_hash, state)
                VALUES ($2, $5, $6, $7)
            ),
            defaults AS (
                -- Retrieve the default sid value from the site_metadata table
                SELECT value
                FROM site_metadata
                WHERE key = 'default'
            ),
            insert_sub_subscriber AS (
                -- Insert a new row into the sub_subscriber table
                INSERT INTO sub_subscriber (uid, sid, status, time)
                SELECT $2, sid, 1, NOW()
                FROM defaults
            ),
            update_sub AS (
                -- Update the sub table by incrementing the subscribers count
                UPDATE sub
                SET subscribers = subscribers + 1
                FROM defaults
                WHERE sub.sid = defaults.value
            )
            -- Insert rows into the user_metadata table
            INSERT INTO user_metadata (uid, key, value)
            VALUES
                ($2, 'invitecode', (SELECT code FROM update_invite_code)),
                ($2, 'nsfw', '1'),
                ($2, 'nsfw_blur', '1')
            "#,
        )
        .bind(code)
        .bind(uid)
        .bind(username)
        .bind(state_code)
        .bind(email)
        .bind(parsed_hash)
        .bind(Json(state))
        .execute(&mut tx)
        .await
        .map_err(|_| UserError::DBError)?
        .rows_affected();

        if affected == 0 {
            tx.rollback().await.map_err(|_| UserError::DBError)?;
            return Err(UserError::BadCode);
        }

        tx.commit().await.map_err(|_| UserError::DBError)?;

        Ok(())
    }

    #[tracing::instrument(skip(self))]
    async fn update_user(&self, user: &users::User) -> Result<(), UserError> {
        // Update user_login table and conditionally insert into customer_update_log table
        sqlx::query(
            r#"
                WITH updated_user AS (
                    UPDATE user_login
                    SET email = $1, parsed_hash = $2, state = $3
                    WHERE uid = $4
                    RETURNING email
                ),
                current_email AS (
                    SELECT email
                    FROM user_login
                    WHERE uid = $4
                )
                INSERT INTO customer_update_log (uid, action, value, created, completed, success)
                SELECT $4, 'change_email', $1, CURRENT_TIMESTAMP, NULL, NULL
                FROM updated_user, current_email
                WHERE updated_user.email != current_email.email
            "#,
        )
        .bind(&user.email)
        .bind(&user.parsed_hash)
        .bind(Json(&user.state))
        .bind(&user.uid)
        .execute(&self.sql_pool)
        .await
        .map_err(|_| UserError::DBError)?;

        Ok(())
    }

    #[tracing::instrument(skip(self))]
    async fn delete_user(&self, uid: &str) -> Result<(), UserError> {
        let mut tx = self
            .sql_pool
            .begin()
            .await
            .map_err(|_| UserError::DBError)?;

        // Get the code from the user_metadata table
        let code: String = sqlx::query_scalar(
            r#"
            SELECT value
            FROM user_metadata
            WHERE uid = $1 AND key = 'invitecode'
            "#,
        )
        .bind(uid)
        .fetch_one(&mut tx)
        .await
        .map_err(|_| UserError::DBError)?;

        // Delete rows from the user_metadata table
        sqlx::query(
            r#"
            DELETE FROM user_metadata
            WHERE uid = $1
            "#,
        )
        .bind(uid)
        .execute(&mut tx)
        .await
        .map_err(|_| UserError::DBError)?;

        // Decrement the subscribers count in the sub table
        sqlx::query(
            r#"
            UPDATE sub
            SET subscribers = subscribers - 1
            WHERE sid IN (
                SELECT sid
                FROM sub_subscriber
                WHERE uid = $1
            )
            "#,
        )
        .bind(uid)
        .execute(&mut tx)
        .await
        .map_err(|_| UserError::DBError)?;

        // Delete the row from the sub_subscriber table
        sqlx::query(
            r#"
            DELETE FROM sub_subscriber
            WHERE uid = $1
            "#,
        )
        .bind(uid)
        .execute(&mut tx)
        .await
        .map_err(|_| UserError::DBError)?;

        // Delete the row from the user_login table
        sqlx::query(
            r#"
            DELETE FROM user_login
            WHERE uid = $1
            "#,
        )
        .bind(uid)
        .execute(&mut tx)
        .await
        .map_err(|_| UserError::DBError)?;

        // Delete the row from the public.user table
        sqlx::query(
            r#"
            DELETE FROM public.user
            WHERE uid = $1
            "#,
        )
        .bind(uid)
        .execute(&mut tx)
        .await
        .map_err(|_| UserError::DBError)?;

        // Decrement the used count in the invite_code table
        sqlx::query(
            r#"
            UPDATE invite_code
            SET used = used - 1
            WHERE code = $1
            "#,
        )
        .bind(&code)
        .execute(&mut tx)
        .await
        .map_err(|_| UserError::DBError)?;

        tx.commit().await.map_err(|_| UserError::DBError)?;

        Ok(())
    }
}
