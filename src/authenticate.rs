use std::{fmt::Debug, sync::Arc};

use crate::users::{self, User, UserError};
use serde::Deserialize;
use thiserror::Error;

#[derive(Error, Debug, PartialEq, Eq)]
pub enum AuthenticationError {
    #[error("Error in the users subsystem: {0}")]
    User(#[from] users::UserError),
    #[error("Wrong Password")]
    BadPassword,
    #[error("Unverified Email")]
    Unverified,
}

impl From<pbkdf2::password_hash::Error> for AuthenticationError {
    fn from(e: pbkdf2::password_hash::Error) -> Self {
        match e {
            pbkdf2::password_hash::Error::Password => AuthenticationError::BadPassword,
            e => AuthenticationError::User(UserError::PasswordHashingError(e)),
        }
    }
}

#[derive(Deserialize)]
pub struct Authenticate {
    pub username: String,
    pub password: String,
    pub remember_me: bool,
}

impl Debug for Authenticate {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Authenticate")
            .field("username", &self.username)
            .field("password", &"***")
            .finish()
    }
}

#[tracing::instrument(skip(user_datastore))]
pub async fn authenticate(
    authenticate: Authenticate,
    user_datastore: &Arc<dyn users::DataStore + Send + Sync>,
) -> Result<User, AuthenticationError> {
    let user = user_datastore
        .get_user_by_username(&authenticate.username)
        .await?;
    if let users::UserState::Unverified { .. } = user.state {
        return Err(AuthenticationError::Unverified);
    }

    users::verify_password(&authenticate.password, &user.parsed_hash).await?;

    Ok(user)
}

#[cfg(test)]
mod test {
    use super::*;
    use async_trait::async_trait;

    struct TUserStorage;
    #[async_trait]
    impl users::DataStore for TUserStorage {
        async fn get_user_by_uid(&self, _uid: &str) -> Result<users::User, users::UserError> {
            unimplemented!()
        }
        async fn get_user_by_email(
            &self,
            _email: &str,
        ) -> Result<Vec<users::User>, users::UserError> {
            unimplemented!()
        }

        async fn get_user_by_username(
            &self,
            username: &str,
        ) -> Result<users::User, users::UserError> {
            match username {
                "1" => Ok(users::User { 
                    uid: username.to_string(),
                    username: username.to_string(),
                    email: "nobody@example.com".to_string(),
                    parsed_hash: "$pbkdf2-sha256$i=27500,l=64$FRvTgi6TO896R3kFBsaFRw$6zlHqUVFmOAFgVZMMNrkQmAKVxPph9NPLkGerFIbMpodEIiBqcYSyKG2iLVxlTbVjsB033qq4x9me7+EZrlsAQ".to_string(),
                    state: users::UserState::Verified
                }),
                "2" => Ok(users::User { 
                    uid: username.to_string(),
                    username: username.to_string(),
                    email: "nobody@example.com".to_string(),
                    parsed_hash: "$pbkdf2-sha256$i=27500,l=64$FRvTgi6TO896R3kFBsaFRw$6zlHqUVFmOAFgVZMMNrkQmAKVxPph9NPLkGerFIbMpodEIiBqcYSyKG2iLVxlTbVjsB033qq4x9me7+EZrlsAQ".to_string(),
                    state: users::UserState::Unverified { delete: "".to_string(), code: "".to_string() }
                }),
                "3" => Ok(users::User { 
                    uid: username.to_string(),
                    username: username.to_string(),
                    email: "nobody@example.com".to_string(),
                    parsed_hash: "$pbkdf2-sha256$i=27500,l=64$FRvTgi6TO896R3kFBsaFRw$6zlHqUVFmOAFgVZMMNrkQmAKVxPph9NPLkGerFIbMpodEIiBqcYSyKG2iLVxlTbVjsB033qq4x9me7+EZrlsAQ".to_string(),
                    state: users::UserState::NewEmail { email: "".to_string(), code: "".to_string() }
                }),
                _ => Err(users::UserError::NotFound)
            }
        }

        async fn write_user(
            &self,
            _code: &str,
            _user: &users::User,
        ) -> Result<(), users::UserError> {
            todo!()
        }

        async fn update_user(&self, _user: &users::User) -> Result<(), users::UserError> {
            todo!()
        }

        async fn delete_user(&self, _uid: &str) -> Result<(), users::UserError> {
            todo!()
        }
    }

    #[tokio::test]
    async fn basic_authentication() -> eyre::Result<()> {
        let store = Arc::new(TUserStorage) as _;
        authenticate(
            Authenticate {
                username: "1".to_string(),
                password: "rizer123".to_string(),
                remember_me: false,
            },
            &store,
        )
        .await?;

        authenticate(
            Authenticate {
                username: "3".to_string(),
                password: "rizer123".to_string(),
                remember_me: false,
            },
            &store,
        )
        .await?;

        Ok(())
    }

    #[tokio::test]
    async fn unverified_user() -> eyre::Result<()> {
        let store = Arc::new(TUserStorage) as _;
        let result = authenticate(
            Authenticate {
                username: "2".to_string(),
                password: "rizer123".to_string(),
                remember_me: false,
            },
            &store,
        )
        .await;

        assert_eq!(result, Err(AuthenticationError::Unverified));

        Ok(())
    }

    #[tokio::test]
    async fn wrong_password_authentication() -> eyre::Result<()> {
        let store = Arc::new(TUserStorage) as _;
        let result = authenticate(
            Authenticate {
                username: "1".to_string(),
                password: "rizer1234".to_string(),
                remember_me: false,
            },
            &store,
        )
        .await;

        assert_eq!(result, Err(AuthenticationError::BadPassword));

        Ok(())
    }
}
