use async_trait::async_trait;
use pbkdf2::{
    password_hash::{PasswordHash, PasswordHasher, PasswordVerifier, SaltString},
    Pbkdf2,
};
use rand_core::OsRng;
use serde::{Deserialize, Serialize};
use std::u8;
use thiserror::Error;

use crate::authenticate::AuthenticationError;

#[derive(Error, Debug, PartialEq, Eq)]
pub enum UserError {
    #[error("Unable to find user")]
    NotFound,
    #[error("User already exists")]
    Exists,
    #[error("Invalid State")]
    InvalidState,
    #[error("Password Complexity Error")]
    PasswordComplexity,
    #[error("Internal error due to password hasher: {0}")]
    PasswordHashingError(pbkdf2::password_hash::Error),

    #[error("Database error")]
    DBError,
    #[error("Unable to create user because invite code was invalid")]
    BadCode,
    #[error("Unable to create user because email is banned")]
    BannedEmailDomain,
    #[error("Unable to create user because registration is disabled")]
    RegistrationDisabled,
    #[error("Unable to create user because username is invalid")]
    InvalidUsername,
    #[error("Unable to create user because email is invalid")]
    InvalidEmail,
}

impl From<pbkdf2::password_hash::Error> for UserError {
    fn from(e: pbkdf2::password_hash::Error) -> Self {
        UserError::PasswordHashingError(e)
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
pub struct User {
    pub uid: String,
    pub username: String,
    pub email: String,
    pub parsed_hash: String,
    pub state: UserState,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
pub enum UserState {
    Unverified { delete: String, code: String },
    NewEmail { email: String, code: String },
    Verified,
    Reset { code: String },
    Banned,
    Deleted,
}

#[async_trait]
pub trait DataStore {
    /// Returns a user given the users email.
    async fn get_user_by_email(&self, email: &str) -> Result<Vec<User>, UserError>;
    /// Returns a user given the users username.
    async fn get_user_by_username(&self, username: &str) -> Result<User, UserError>;
    /// Returns a user given the users uid.
    async fn get_user_by_uid(&self, uid: &str) -> Result<User, UserError>;

    /// Writes a user only if the user doesn't already exist in the system. If the user does exist throws an error.

    async fn write_user(&self, code: &str, user: &User) -> Result<(), UserError>;
    /// Updates a user provided that the user already exists.
    async fn update_user(&self, user: &User) -> Result<(), UserError>;

    /// Deletes a user provided that the user already exists.
    async fn delete_user(&self, uid: &str) -> Result<(), UserError>;
}

#[derive(Error, Debug)]
pub enum EmailError {
    #[error("Unable to contact mail server")]
    MailServerOffline,
}

#[derive(Debug, Clone, Copy)]
pub enum ContactType {
    Verify,
    Reset,
    Update,
}

#[async_trait]
pub trait UserContact {
    async fn send_email(
        &self,
        ty: ContactType,
        email: &str,
        uid: &str,
        code: &str,
    ) -> Result<(), EmailError>;
}

pub fn hash_password(password: &str) -> Result<String, UserError> {
    Ok(Pbkdf2
        .hash_password(password.as_bytes(), &SaltString::generate(&mut OsRng))?
        .to_string())
}

#[tracing::instrument(skip(password, hash))]
pub async fn verify_password(password: &str, hash: &str) -> Result<(), AuthenticationError> {
    let password = password.to_string();
    let hash = hash.to_string();

    tokio::task::spawn_blocking(move || {
        let parsed_hash = PasswordHash::new(&hash)?;
        Pbkdf2
            .verify_password(password.as_bytes(), &parsed_hash)
            .map_err(|_| AuthenticationError::BadPassword)
    })
    .await
    .expect("This should only happen on cancel/panic")
}

use tracing::error;
