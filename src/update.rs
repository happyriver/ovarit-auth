use std::{fmt::Debug, sync::Arc};

use crate::{
    authenticate::AuthenticationError,
    users::{self, EmailError, User, UserState},
};
use serde::Deserialize;
use thiserror::Error;
use uuid::Uuid;

#[derive(Error, Debug)]
pub enum ResetError {
    #[error("Error in the users subsystem: {0}")]
    User(#[from] users::UserError),
    #[error("Error in the contact subsystem: {0}")]
    Contact(#[from] EmailError),
    #[error("Error in the authentication subsystem: {0}")]
    Authenticate(#[from] AuthenticationError),
}

#[derive(Deserialize, Debug)]
pub struct ResetRequest {
    username: String,
}

/// Starts the reset process, sends an email to the given email address with a link to the reset page.
/// Email is parameterized with the "magic code" rather than a full link.
#[tracing::instrument(skip(user_datastore, user_contact))]
pub async fn start_reset(
    req: ResetRequest,
    user_datastore: &Arc<dyn users::DataStore + Send + Sync>,
    user_contact: &Arc<dyn users::UserContact + Send + Sync>,
) -> Result<(), ResetError> {
    let code = Uuid::new_v4().as_hyphenated().to_string();
    let user = user_datastore.get_user_by_username(&req.username).await?;
    if !matches!(user.state, UserState::Verified | UserState::Reset { .. }) {
        return Err(ResetError::User(users::UserError::InvalidState));
    }
    let user = User {
        state: UserState::Reset { code: code.clone() },
        ..user
    };
    user_datastore.update_user(&user).await?;
    user_contact
        .send_email(
            users::ContactType::Reset,
            &user.email,
            &user.username,
            &code,
        )
        .await?;
    Ok(())
}

#[derive(Deserialize)]
pub struct CompleteReset {
    username: String,
    code: String,
    new_password: String,
}

impl Debug for CompleteReset {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("CompleteReset")
            .field("uid", &self.username)
            .field("code", &self.code)
            .field("new_password", &"***")
            .finish()
    }
}

#[tracing::instrument(skip(user_datastore))]
pub async fn complete_reset(
    req: CompleteReset,
    user_datastore: &Arc<dyn users::DataStore + Send + Sync>,
) -> Result<(), ResetError> {
    let user = user_datastore.get_user_by_username(&req.username).await?;
    let user = match user.state {
        UserState::Reset { code } if code == req.code => User {
            state: UserState::Verified,
            parsed_hash: users::hash_password(&req.new_password)?,
            ..user
        },
        _ => return Err(ResetError::User(users::UserError::InvalidState)),
    };
    user_datastore.update_user(&user).await?;
    Ok(())
}

#[derive(Deserialize)]
pub struct UpdateRequest {
    pub uid: String,
    pub password: String,
    pub new_email: Option<String>,
    pub new_password: Option<String>,
}

impl Debug for UpdateRequest {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("UpdateRequest")
            .field("uid", &self.uid)
            .field("password", &"***")
            .field("new_email", &self.new_email)
            .field("new_password", &"***")
            .finish()
    }
}

#[tracing::instrument(skip(user_datastore, user_contact))]
pub async fn update(
    req: UpdateRequest,
    user_datastore: &Arc<dyn users::DataStore + Send + Sync>,
    user_contact: &Arc<dyn users::UserContact + Send + Sync>,
) -> Result<(), ResetError> {
    let user = user_datastore.get_user_by_uid(&req.uid).await?;
    println!("User: {:?}", user);
    users::verify_password(&req.password, &user.parsed_hash).await?;
    println!("Password verified");
    let code = Uuid::new_v4().as_hyphenated().to_string();
    let use_new_email = if let Some(new_email) = dbg!(req.new_email.as_ref()) {
        new_email != &user.email
    } else {
        false
    };
    println!("Use new email: {}", use_new_email);

    let user = User {
        state: if use_new_email {
            UserState::NewEmail {
                email: req.new_email.clone().unwrap(),
                code: code.clone(),
            }
        } else {
            UserState::Verified
        },
        parsed_hash: if let Some(new_password) = req.new_password {
            users::hash_password(&new_password)?
        } else {
            user.parsed_hash
        },
        ..user
    };

    user_datastore.update_user(&user).await?;
    if use_new_email {
        user_contact
            .send_email(
                users::ContactType::Update,
                &req.new_email.unwrap(),
                &user.username,
                &code,
            )
            .await?;
    }
    Ok(())
}

#[derive(Deserialize, Debug)]
pub struct DeleteRequest {
    pub uid: String,
    pub code: String,
}

#[tracing::instrument(skip(user_datastore))]
pub async fn delete(
    req: DeleteRequest,
    user_datastore: &Arc<dyn users::DataStore + Send + Sync>,
) -> Result<(), ResetError> {
    let user = user_datastore.get_user_by_uid(&req.uid).await?;

    let is_valid_code = match user.state {
        UserState::Unverified { delete, .. } => delete == req.code,
        _ => return Err(ResetError::User(users::UserError::InvalidState)),
    };

    if !is_valid_code {
        return Err(ResetError::User(users::UserError::InvalidState));
    }

    user_datastore.delete_user(&req.uid).await?;
    Ok(())
}

#[cfg(test)]
mod test {
    use std::{collections::HashMap, sync::Arc};

    use tokio::sync::Mutex;

    use crate::users::{self, DataStore, User, UserContact, UserError};

    use super::UpdateRequest;

    struct HashMapStore {
        map: Mutex<HashMap<String, User>>,
    }
    #[async_trait::async_trait]
    impl DataStore for HashMapStore {
        async fn get_user_by_uid(&self, uid: &str) -> Result<User, UserError> {
            self.map
                .lock()
                .await
                .get(uid)
                .cloned()
                .ok_or(UserError::NotFound)
        }

        async fn get_user_by_email(
            &self,
            _email: &str,
        ) -> Result<Vec<users::User>, users::UserError> {
            Ok(self
                .map
                .lock()
                .await
                .iter()
                .filter(|(_, user)| user.email == _email)
                .map(|(_, user)| user.clone())
                .collect())
        }

        async fn get_user_by_username(&self, uid: &str) -> Result<users::User, UserError> {
            self.map
                .lock()
                .await
                .iter()
                .find(|(_, user)| user.username == uid)
                .map(|(_, user)| user.clone())
                .ok_or(UserError::NotFound)
        }

        async fn write_user(&self, _code: &str, user: &users::User) -> Result<(), UserError> {
            let mut map = self.map.lock().await;
            if map
                .values()
                .any(|stored_user| user.email == stored_user.email)
            {
                return Err(UserError::Exists);
            }
            map.insert(user.uid.clone(), user.clone());

            Ok(())
        }

        async fn update_user(&self, user: &users::User) -> Result<(), UserError> {
            let mut map = self.map.lock().await;

            let old_user = map.get_mut(&user.uid).ok_or(UserError::NotFound)?;

            *old_user = user.clone();

            Ok(())
        }

        async fn delete_user(&self, uid: &str) -> Result<(), UserError> {
            let mut map = self.map.lock().await;
            map.remove(uid);
            Ok(())
        }
    }

    struct HashMapContact {
        map: Mutex<HashMap<String, String>>,
    }
    #[async_trait::async_trait]
    impl UserContact for HashMapContact {
        async fn send_email(
            &self,
            _ty: users::ContactType,
            _email: &str,
            uid: &str,
            code: &str,
        ) -> Result<(), users::EmailError> {
            self.map
                .lock()
                .await
                .insert(uid.to_string(), code.to_string());
            Ok(())
        }
    }

    #[tokio::test]
    async fn test_update() {
        let store = Arc::new(HashMapStore {
            map: Mutex::new(HashMap::new()),
        });
        let contact = Arc::new(HashMapContact {
            map: Mutex::new(HashMap::new()),
        });

        // create a user and store it
        let user = users::User {
            uid: "test".to_string(),
            username: "test".to_string(),
            email: "test@test.test".to_string(),
            state: users::UserState::Verified,
            parsed_hash: users::hash_password("test").unwrap(),
        };
        store.write_user("", &user).await.unwrap();

        // update the user
        let update = UpdateRequest {
            uid: "test".to_string(),
            password: "test".to_string(),
            new_email: Some("test2@test.test".to_string()),
            new_password: Some("test2".to_string()),
        };
        super::update(update, &(store.clone() as _), &(contact.clone() as _))
            .await
            .unwrap();

        // check that the user was updated
        let updated_user = store.get_user_by_username("test").await.unwrap();
        assert_eq!(updated_user.email, "test2@test.test");
        assert!(users::verify_password("test2", &updated_user.parsed_hash)
            .await
            .is_ok());

        // check that the user was contacted
        let code = contact.map.lock().await.get("test").cloned().unwrap();
        assert_eq!(code, "");
    }
}
