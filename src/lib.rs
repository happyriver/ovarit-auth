//! Library to support the basic operations for a auth system
//! Must be able to:
//!   * create a user
//!   * resend a validation email
//!   * validate an email
//!   * authenticate a user
//!   * reset a password
//!   * update a password/email

pub mod authenticate;

pub mod create;

pub mod update;

pub mod users;

pub mod flask_session;
