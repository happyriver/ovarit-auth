use async_trait::async_trait;
use axum::{
    extract::FromRequestParts,
    headers::Cookie,
    http::{request::Parts, StatusCode},
    RequestPartsExt, TypedHeader,
};
use base64::Engine;
use flate2::{read::ZlibDecoder, write::ZlibEncoder, Compression};
use hmac::{Hmac, Mac, SimpleHmac};
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};
use sha1::Sha1;
use sha2::Sha512;
use std::{
    fmt::Debug,
    io::Read,
    io::Write,
    time::{Duration, SystemTime},
};
use tracing::error;

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq, Eq)]
pub struct Session {
    pub _fresh: bool,
    pub _id: Option<String>,
    pub _user_id: String,
    pub csrf_token: String,
    pub language: Option<String>,
    #[serde(default)]
    pub remember_me: bool,
}

#[derive(Debug, Deserialize, Clone)]
pub struct User {
    pub uid: String,
    pub email: Option<String>,
    pub name: String,
}

static ONE_MONTH: Duration = Duration::from_secs(60 * 60 * 24 * 30);

#[async_trait]
impl<S> FromRequestParts<S> for Session {
    type Rejection = (StatusCode, &'static str);

    async fn from_request_parts(parts: &mut Parts, _state: &S) -> Result<Self, Self::Rejection> {
        let cookie: Option<TypedHeader<Cookie>> = parts.extract().await.unwrap();

        let session_cookie = cookie.as_ref().and_then(|cookie| cookie.get("session"));

        // return the new created session cookie for client
        let Some(session_cookie) = session_cookie else {
            return Err((StatusCode::UNAUTHORIZED, "No session cookie found in request"))
        };

        decode_and_check(session_cookie.as_bytes(), ONE_MONTH.as_secs())
    }
}

type TokenError = (StatusCode, &'static str);
type TokenResult<S> = Result<S, TokenError>;
type HmacSha1 = SimpleHmac<Sha1>;

const URL_SAFE_ENGINE: base64::engine::GeneralPurpose = base64::engine::GeneralPurpose::new(
    &base64::alphabet::URL_SAFE,
    base64::engine::general_purpose::NO_PAD,
);

static APP_SECRET_KEY: Lazy<String> =
    Lazy::new(|| std::env::var("APP_SECRET_KEY").expect("APP_SECRET_KEY must be set"));

static SESSIONS_SIGNING_KEY: Lazy<Vec<u8>> = Lazy::new(|| {
    let secret_key = &APP_SECRET_KEY;
    let mut mac = HmacSha1::new_from_slice(secret_key.trim().as_bytes()).expect("");
    mac.update(b"cookie-session");
    let result = mac.finalize();
    result.into_bytes().to_vec()
});

pub fn compress_payload(payload: &str) -> Result<Vec<u8>, Box<dyn std::error::Error>> {
    let mut encoder = ZlibEncoder::new(Vec::new(), Compression::default());
    encoder.write_all(payload.as_bytes())?;
    let compressed_payload = encoder.finish()?;
    Ok(compressed_payload)
}

pub fn decompress_payload(compressed_payload: &[u8]) -> Result<String, Box<dyn std::error::Error>> {
    let mut decoder = ZlibDecoder::new(compressed_payload);
    let mut payload = String::new();
    decoder.read_to_string(&mut payload)?;
    Ok(payload)
}

pub fn encode_session(session: &Session) -> TokenResult<String> {
    let payload = serde_json::to_string(&session)
        .map_err(|_| (StatusCode::INTERNAL_SERVER_ERROR, "This is widly unlikely"))?;

    let compressed_payload = compress_payload(&payload)
        .map_err(|_| (StatusCode::INTERNAL_SERVER_ERROR, "failed to gzip"))?;

    let parts = format!(
        ".{}.{}",
        URL_SAFE_ENGINE.encode(compressed_payload),
        URL_SAFE_ENGINE.encode(u64::to_be_bytes(
            SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .expect("We can't be before EPOCH")
                .as_secs()
        )),
    );

    let signature = sign(parts.as_bytes());

    let cookie_value = format!("{}.{}", parts, URL_SAFE_ENGINE.encode(signature),);

    Ok(cookie_value)
}

pub fn decode_and_check(jws: &[u8], time_padding: u64) -> TokenResult<Session> {
    // 1. Split the string into its parts
    let mut parts = jws.rsplitn(2, |v| v == &b'.');
    let sig = parts.next().ok_or_else(|| bad_format("Expected a sig"))?;
    let signed = parts
        .next()
        .ok_or_else(|| bad_format("Expected rest of value"))?;

    // 2. Split the signed part into its parts
    let mut parts = signed.rsplitn(2, |v| v == &b'.');
    let encoded_timestamp = parts
        .next()
        .ok_or_else(|| bad_format("Expected a timestamp"))?;

    // 3. Decode the timestamp and check it is in the valid range
    check_timestamp(decode(encoded_timestamp)?.as_slice(), time_padding)?;

    // 4. Decode the rest of the signed part
    let val = parts.next().ok_or_else(|| bad_format("Expected a value"))?;

    // 5. Check that the signature is valid
    check_sig(decode(sig)?.as_slice(), signed)?;

    // 6. Decode the rest of the signed part
    decode_data(val)
}

fn check_timestamp(encoded_timestamp: &[u8], time_padding: u64) -> TokenResult<()> {
    let timestamp = if encoded_timestamp.len() == 4 {
        // 32 bit number out of Python
        u32::from_be_bytes(encoded_timestamp.try_into().map_err(bad_format)?) as u64
    } else {
        u64::from_be_bytes(encoded_timestamp.try_into().map_err(bad_format)?)
    };

    if SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .expect("We can't be before EPOCH")
        > Duration::from_secs(timestamp + time_padding)
    {
        error!(
            "Timestamp too old: {:?} + padding {:?} = {:?}",
            timestamp,
            time_padding,
            timestamp + time_padding
        );
        Err((StatusCode::UNAUTHORIZED, "Token too old"))
    } else {
        Ok(())
    }
}

fn check_sig(sig: &[u8], parts: &[u8]) -> TokenResult<()> {
    let bytes = sign(parts);
    if bytes.as_slice() != sig {
        println!("parts: {:?}", String::from_utf8(parts.to_vec()));
        error!("input sig didn't match calculated sig");
        Err((StatusCode::UNAUTHORIZED, "Bad Signature"))
    } else {
        Ok(())
    }
}

pub fn sign(parts: &[u8]) -> Vec<u8> {
    let mut mac = HmacSha1::new_from_slice(SESSIONS_SIGNING_KEY.as_slice())
        .expect("Works with variable sized keys");
    mac.update(parts);
    let result = mac.finalize();
    result.into_bytes().to_vec()
}

fn bad_format<T: Debug>(e: T) -> TokenError {
    error!("Poorly formatted session token: {:?}", e);
    (StatusCode::UNAUTHORIZED, "Improper Format")
}

fn decode(input: &[u8]) -> TokenResult<Vec<u8>> {
    let mut compressed = Vec::new();
    URL_SAFE_ENGINE
        .decode_vec(input, &mut compressed)
        .map_err(bad_format)?;

    Ok(compressed)
}

fn decode_data(val: &[u8]) -> TokenResult<Session> {
    let zip = val[0] == b'.';
    let val = if zip { &val[1..] } else { val };
    let mut decoded_data = Vec::new();
    let val = if zip {
        let val = decode(val)?;
        let blah = decompress_payload(&val)
            .map_err(|_| (StatusCode::INTERNAL_SERVER_ERROR, "failed to ungzip"))?;
        decoded_data.extend(blah.as_bytes());
        decoded_data.as_slice()
    } else {
        decoded_data = decode(val)?;
        decoded_data.as_slice()
    };

    let mut session: Session = serde_json::from_slice(val).map_err(bad_format)?;

    if let Some((id, _reset_count)) = session._user_id.split_once('$') {
        session._user_id = id.to_string();
    };

    Ok(session)
}

pub fn remember_me(uid: String) -> String {
    let mut hmac = Hmac::<Sha512>::new_from_slice(APP_SECRET_KEY.as_bytes()).unwrap();
    hmac.update(uid.as_bytes());
    let result = hmac.finalize().into_bytes().to_vec();
    let encode = hex::encode(result);
    format!("{}|{}", uid, encode)
}

#[cfg(test)]
mod test {
    use super::*;
    static ONE_MONTH: u64 = 2678400;

    // Timestamp stored in this value is: 1607479410 as a 32 bit int
    static TOKEN: &str = r#".eJwlj0tqxEAMRO9isoxB_VFLPZcxklqahIwnYI9XIXdPQ5YqeFVPP8sWh58fy-11XP6-bJ9juS0E0BvrkBrGmRUoWMaAzOYlHJ0JvecyGtCQUTywtZLINEtOuXpTbUmQMxYRGFwDayGKZqLcu5dqpVkU6aVrQC2dUI2DyqxXW6bIdfrxbwMJTQbjakNsrdVh7dBoFU0dO9UBGG9pMnYesb2-v_w5qamRlCpK5ywBEhQJVHOab6lb5qmfcdDkHvK8X3L3Sc3r8N13neP7DEIep__-AX88VTU.X9Awcg.5CCPisQH8Y0XwsidMCzL_z_uYUY"#;

    #[test]
    fn roundtrip_cookie() -> TokenResult<()> {
        std::env::set_var("APP_SECRET_KEY", "super secret key");

        let session = Session {
            _user_id: "1".to_string(),
            csrf_token: "2".to_string(),
            _fresh: true,
            _id: None,
            language: None,
            remember_me: false,
        };

        let encoded = encode_session(&session)?;
        let decoded = decode_and_check(encoded.as_bytes(), ONE_MONTH)?;

        assert_eq!(session, decoded);

        Ok(())
    }

    #[test]
    fn decode_cookie() -> TokenResult<()> {
        std::env::set_var("APP_SECRET_KEY", "super secret key");

        let a = decode_and_check(
            TOKEN.as_bytes(),
            SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_secs(),
        )?;
        println!("{:?}", a);

        Ok(())
    }

    #[test]
    fn remember_me() -> TokenResult<()> {
        std::env::set_var("APP_SECRET_KEY", r#"super secret key"#);

        let rm = "4d8c8abb-98ae-41bb-9e57-b480bfefd1c8|1899285df9dfa94f0ab710a5ecae5d9d99510a27c65699c8b58d9b038e56dea7d7f8547d8165b6cdaacf55d45db35c84f0f2eb2e8e0dfaefa6b2ea875780b013";
        let (payload, _sig) = rm.split_once('|').unwrap();
        let cookie = super::remember_me(payload.to_string());

        assert_eq!(rm, cookie.as_str());

        Ok(())
    }

    #[test]
    #[ignore]
    fn real_life() -> TokenResult<()> {
        let res = decode_and_check("eyJfZnJlc2giOmZhbHNlLCJjc3JmX3Rva2VuIjoiNjQ3YTg5Nzk0ZWNiZTE1OGViZDAzMTQwN2YxMzdlNzNjZDBjN2ZkYSIsImxhbmd1YWdlIjoiIiwiX3VzZXJfaWQiOiI3N2E2YzFhOC04M2FiLTQ3M2ItOTYwOS1iYTY1ZTcyNzc5MzgkMCJ9.YdIUlw.I7Z2csCpm-Di6cA4HPS59aJBzvM".as_bytes(), 
            SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_secs(),
    )?;
        println!("Session: {:?}", res);

        Ok(())
    }

    #[test]
    #[ignore = "OnceCell"]
    fn generate_secret_key() -> TokenResult<()> {
        std::env::set_var("APP_SECRET_KEY", "secret-key");

        let moodle_value: &[i8] = &[
            -57, -8, 104, -87, -91, 56, -35, 87, -109, -89, -120, -96, -3, -57, -1, -52, 112, 45,
            -37, 80,
        ];
        let u8value = unsafe { &*(moodle_value as *const _ as *const [u8]) };

        assert_eq!(SESSIONS_SIGNING_KEY.as_slice(), u8value);

        Ok(())
    }

    #[test]
    #[ignore = "OnceCell"]
    fn check_sig_value() -> TokenResult<()> {
        std::env::set_var("APP_SECRET_KEY", "secret-key");

        let moodle_value: &[i8] = &[
            1, -21, -80, 54, 68, 77, -50, 96, -30, -52, 115, 63, 44, -40, -67, -123, -107, 52, 117,
            -78,
        ];
        let u8value = unsafe { &*(moodle_value as *const _ as *const [u8]) };
        check_sig(u8value, "value".as_bytes())?;

        Ok(())
    }
}
